import com.basho.riak.client.api.RiakClient;
import com.basho.riak.client.api.commands.kv.StoreValue;
import com.basho.riak.client.core.query.Location;
import com.basho.riak.client.core.query.Namespace;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutionException;

public class StoreString {
    public static void main(String [] args) throws UnknownHostException, ExecutionException, InterruptedException {

        RiakClient client = RiakClient.newClient(10027, "127.0.0.1");
        Location location = new Location(new Namespace("TestBucket"),"TestKey");
        String data = " this is my data";

        StoreValue sv = new StoreValue.Builder(data).withLocation(location).build();
        StoreValue.Response svResponse = client.execute(sv);

        client.shutdown();
    }
}
