>1. [install riak](http://docs.basho.com/riak/kv/2.1.4/setup/installing/) 
>2. [run a cluster](http://docs.basho.com/riak/kv/2.1.4/using/cluster-operations/adding-removing-nodes/)
>(in this part I feel quite strange because five nodes have already run, but in the command line I cannot use command line, it shows nodes not running.
>![](https://lh3.googleusercontent.com/-ibX0FO26SfM/V1BRflNqCmI/AAAAAAAAAHQ/FMpZSt7YT5ol7Zn4FdJ5z1yHAbXCupTPgCCo/s914/Screen%2BShot%2B2016-06-02%2Bat%2B5.31.45%2BPM.png)
>but its actually running on port localhost 8098, 10018, 10028, 10038, 10048 and 10058
>![](https://lh3.googleusercontent.com/-Xf-tPEfXLeo/V1BSMJ2xCQI/AAAAAAAAAHc/GPt0qYbxDBYethemHNVMfDKYwmQNZR5AgCCo/s912/Screen%2BShot%2B2016-06-02%2Bat%2B5.34.23%2BPM.png)
>In addition, I also try to install [docker-riak](https://github.com/hectcastro/docker-riak), this one works fine.  

>2. [create a java client store and fetch data](https://github.com/basho/riak-java-client/wiki/Cookbook-2.0)
>I can store things in the riak nodes follow the wiki, like picture and string.
>![](https://lh3.googleusercontent.com/-dIFbIY0TJno/V1BRl41gSiI/AAAAAAAAAHk/FmUrYsCzeo8WDuZ9bA1krobVnQH4bkOsgCCo/s576/Screen%2BShot%2B2016-06-02%2Bat%2B5.30.13%2BPM.png)
>![](https://lh3.googleusercontent.com/-mU1iqsCCKGw/V1BRetOe1rI/AAAAAAAAAHk/Ji8WMCddyh0k9_CqKEmh24F9MUTBUR-nQCCo/s800/Screen%2BShot%2B2016-06-02%2Bat%2B5.30.28%2BPM.png)
>![](https://lh3.googleusercontent.com/-XKrLwxeFC6A/V1BRevS1zZI/AAAAAAAAAHk/kvHqliuxkVQtHC7ByWvXLhfCyZoijwKkQCCo/s576/Screen%2BShot%2B2016-06-02%2Bat%2B5.30.07%2BPM.png)  
>To run that code, we need another two external libraries show in the photo. It's already included in the code. Since I use intellij create the project, maybe you need to change something migrating the project.  
>All things are in the wiki, so all I have done these days is only try snippets.
>![](https://lh3.googleusercontent.com/-pfmMHiv-Ico/V1BTVXXBzsI/AAAAAAAAAH0/-b7LhiH-O_MJF90m0cqq4gsq_9bbrquYQCLcB/h240/Screen%2BShot%2B2016-06-02%2Bat%2B5.39.40%2BPM.png)